## Use an official Fedora 31 runtime as a base image
FROM fedora:31
ENV container docker



# Author
LABEL maintainer="Ivan Gregoretti"



# Set the working directory to /opt
WORKDIR /opt



# Copy the current directory contents into the container at /opt
ADD . /opt



# Create users

RUN useradd -m -u 1000 -d /home/fedora   -s /bin/bash -c "Fedora"           fedora   && usermod -a -G wheel fedora   && \
    useradd -m -u 6799 -d /home/igregore -s /bin/bash -c "Ivan Gregoretti"  igregore && usermod -a -G wheel igregore && \
    useradd -m -u 6829 -d /home/csimpson -s /bin/bash -c "Claire Simpson"   csimpson && usermod -a -G wheel csimpson && \
    \
    mkdir        /home/igregore/Downloads && chown -R igregore:igregore /home/igregore/Downloads && \
    mkdir -m 700 /home/igregore/.ssh      && chown -R igregore:igregore /home/igregore/.ssh      && \
    mkdir        /home/igregore/.aws      && chown -R igregore:igregore /home/igregore/.aws      && \
    \
    \
    sed -i -e "\$afastestmirror=true" /etc/dnf/dnf.conf && \
    dnf install -y --nogpgcheck \
    http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-31.noarch.rpm \
    http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-31.noarch.rpm && \
    dnf install -y dnf-plugins-core redhat-rpm-config && \
    dnf clean all \
    \
    \
    && dnf update -y && dnf clean all \
    \
    \
    && dnf install -y \
    sudo exa vim htop iftop iotop nethogs collectl wget \
    s3fs-fuse fuse-sshfs fuse-encfs nfs-utils bindfs \
    git bash-completion nodejs \
    jq python3-csvkit \
    && dnf clean all && \
    \
    \
    curl 'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip' -o 'awscliv2.zip' && \
    unzip awscliv2.zip && \
    ./aws/install && \
    sed -i '$ a complete -C "/usr/local/bin/aws_completer" aws' /home/igregore/.bashrc && \
    \
    \
    setcap "cap_net_admin,cap_net_raw+pe" /usr/sbin/nethogs && \
    chmod 640 /etc/sudoers && sed --in-place -e 's|#\s*\(%wheel\s*ALL=(ALL)\s*NOPASSWD:\s*ALL\)|\1\nDefaults lecture=never|' /etc/sudoers && chmod 440 /etc/sudoers && \
    sed --in-place -e 's|#\s*user_allow_other|user_allow_other|' /etc/fuse.conf && \
    \
    npm install -g json2csv



# Re-set the working directory to something more convenient
WORKDIR /home/igregore



CMD ["tail", "-f", "/dev/null"]
