# Fedora Fern

Author: Ivan Gregoretti, PhD.

Fedora Fern is a minimal linux OS environment created to facilitate operations
with AWS and Git at the command line.



## Building the container Fedora Fern

    docker build -t compbio-research/fedorafern:0.1.0 /base/my/script2/fedorafern



## Using the Fedora Fern container

### Run the Fedora Fern container

Run it in the background and pass it the AWS command line interface configuration directory

    docker run -d --rm \
    -v /home/igregore/.aws:/home/igregore/.aws \
    -u 6799:6799 --name fedorafern compbio-research/fedorafern:0.1.0

Run it passing configuration for AWS CLI and configuration and a working directory (src) for Git

    docker run -d --rm \
    -v /home/igregore/.aws:/home/igregore/.aws \
    -v /home/igregore/.gitconfig:/home/igregore/.gitconfig \
    -v /home/igregore/.ssh:/home/igregore/.ssh \
    -v /home/igregore/src:/home/igregore/src \
    -u 6799:6799 --name fedorafern compbio-research/fedorafern:0.1.0


### Open a Bash shell and operate withing the container

Execute the command with the flag `-it` to make it interactive.

    docker exec -it -u igregore fedorafern bash
